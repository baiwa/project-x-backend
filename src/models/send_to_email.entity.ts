import { Entity, Column, PrimaryGeneratedColumn, Long, Timestamp } from 'typeorm';

@Entity({ name: "send_to_email" })
export class SendToEmail {
    @PrimaryGeneratedColumn()
    sendToEmailId: Long

    @Column({ type: 'int', name: 'leave_email_id' })
    leaveEmailId: Long;

    @Column({ type: 'varchar', name: 'send_to_user' })
    sendToUser: String;

}