import { Entity, Column, PrimaryGeneratedColumn, Long } from 'typeorm';
// { name: "fw_users" }
@Entity({ name: "fw_users_img_profile" })
export class FwUsersImgProfile {
    @PrimaryGeneratedColumn()
    img_profile_id: Long

    @Column({ type: 'varchar', name: 'username' })
    username: string;

    @Column({ type: 'varchar', name: 'img_profile' })
    imgProfile: string;

    @Column({ type: 'varchar', name: 'created_by', nullable: true })
    createdBy: string;

    @Column({ type: 'datetime', name: 'created_date', nullable: true })
    createdDate: Date;

    @Column({ type: 'varchar', name: 'updated_by', nullable: true })
    updatedBy: string;

    @Column({ type: 'datetime', name: 'updated_date', nullable: true })
    updatedDate: Date;

    @Column({ type: 'varchar', name: 'is_deleted', default: "N" })
    isDeleted: string;

}