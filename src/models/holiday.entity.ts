import { Entity, Column, PrimaryGeneratedColumn, Long } from 'typeorm';
import { CommonColumn } from 'src/common/model/main_model';

@Entity({ name: 'holiday' })
export class Holiday extends CommonColumn {
    @PrimaryGeneratedColumn()
    holiday_id: Long

    @Column({ type: 'datetime', name: 'holiday_day' })
    holidayDay: Date;

    @Column({ type: 'varchar', name: 'holiday_description', nullable: true })
    holidayDescription: string;

    @Column({ type: 'varchar', name: 'active', nullable: true, default: 'Y' })
    active: string;

}