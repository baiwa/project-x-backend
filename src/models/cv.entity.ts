import { Entity, Column, PrimaryGeneratedColumn, Long } from 'typeorm';

@Entity()
export class CV {
    @PrimaryGeneratedColumn()
    cv_id: Long

    @Column({ type: 'varchar', name: 'username', length: 20 })
    username: string;

    @Column({ type: 'varchar', name: 'filename', nullable: true })
    filename: string;

    @Column({ type: 'varchar', name: 'name_in_server', nullable: true })
    nameInServer: string;

    @Column({ type: 'varchar', name: 'created_by', nullable: true })
    createdBy: string;

    @Column({ type: 'datetime', name: 'created_date', nullable: true })
    createdDate: Date;

    @Column({ type: 'varchar', name: 'is_deleted', default: "N" })
    isDeleted: string;

}