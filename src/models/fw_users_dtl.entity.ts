import { Entity, Column, PrimaryGeneratedColumn, Long } from 'typeorm';
// { name: "fw_users" }
@Entity({ name: "fw_users_detail" })
export class FwUsersDetail {
    @PrimaryGeneratedColumn()
    fwuser_dtl_id: Long
 
    @Column({ type: 'varchar', name: 'username', length: 30 })
    username: string;

    @Column({ type: 'varchar', name: 'email', length: 50 })
    email: string;

    @Column({ type: 'varchar', name: 'company', nullable: true })
    company: string;

    @Column({ type: 'varchar', name: 'phone', nullable: true })
    phone: string;

    @Column({ type: 'varchar', name: 'first_name', nullable: true })
    firstName: string;

    @Column({ type: 'varchar', name: 'last_name', nullable: true })
    lastName: string;

    @Column({ type: 'varchar', name: 'job_position', nullable: true })
    job_position: string;

    @Column({ type: 'varchar', name: 'created_by', nullable: true })
    createdBy: string;

    @Column({ type: 'datetime', name: 'created_date', nullable: true })
    createdDate: Date;

    @Column({ type: 'varchar', name: 'updated_by', nullable: true })
    updatedBy: string;

    @Column({ type: 'datetime', name: 'updated_date', nullable: true })
    updatedDate: Date;

    @Column({ type: 'varchar', name: 'is_deleted', default: "N" })
    isDeleted: string;

}