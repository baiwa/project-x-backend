import { Entity, Column, PrimaryGeneratedColumn, Long, Timestamp } from 'typeorm';
// { name: "fw_users" }
@Entity({ name: "leave_emails" })
export class LeaveEmail {
    @PrimaryGeneratedColumn()
    leave_email_id: Long

    @Column({ type: 'varchar', name: 'send_by', unique: false })
    sendBy: string;

    @Column({ type: 'varchar', name: 'file_upload', unique: false })
    fileUpload: string;

    @Column({ type: 'varchar', name: 'file_upload_name', unique: false })
    fileUploadName: string;

    @Column({ type: 'varchar', name: 'title' , unique: false})
    title: string;

    @Column({ type: 'varchar', name: 'leave_type' , unique: false})
    leaveType: string;

    @Column({ type: 'varchar', name: 'date_type', unique: false })
    dateType: string;

    @Column({ type: 'datetime', name: 'start_date' , unique: false})
    startDate: Date;

    @Column({ type: 'datetime', name: 'end_date', unique: false })
    endDate: Date;

    @Column({ type: 'varchar', name: 'detail', nullable: true , unique: false})
    detail: string;

    @Column({ type: 'varchar', name: 'created_by', nullable: true, unique: false })
    createdBy: string;

    @Column({ type: 'datetime', name: 'created_date', nullable: true, unique: false })
    createdDate: Date;

    @Column({ type: 'varchar', name: 'is_deleted', default: "N", unique: false })
    isDeleted: string;

}