import { Entity, Column, PrimaryGeneratedColumn, Long } from 'typeorm';
// { name: "fw_users" }
@Entity()
export class FwUsers {
    @PrimaryGeneratedColumn()
    fwuser_id: Long

    @Column({ type: 'varchar', name: 'username', length: 20, primary: false, unique: false })
    username: string;

    @Column({ type: 'varchar', name: 'user_rule', nullable: true })
    userRule: string;

    @Column({ type: 'varchar', name: 'password', nullable: true })
    password: string;

    @Column({ type: 'varchar', name: 'created_by', nullable: true })
    createdBy: string;

    @Column({ type: 'datetime', name: 'created_date', nullable: true })
    createdDate: Date;

    @Column({ type: 'varchar', name: 'updated_by', nullable: true })
    updatedBy: string;

    @Column({ type: 'datetime', name: 'updated_date', nullable: true })
    updatedDate: Date;

    @Column({ type: 'varchar', name: 'is_deleted', default: "N" })
    isDeleted: string;

}

// CREATE TABLE `fw_users` (
//     `username` varchar(20) NOT NULL,
//     `user_rule` varchar(255) NOT NULL,
//     `password` varchar(255) NOT NULL,
//     `created_by` varchar(255) NOT NULL,
//     `created_date` datetime NOT NULL,
//     `updated_by` varchar(255) NOT NULL,
//     `updated_date` datetime NOT NULL,
//     `is_deleted` varchar(255) NOT NULL DEFAULT 'N',
//     PRIMARY KEY (`username`)
//   ) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8