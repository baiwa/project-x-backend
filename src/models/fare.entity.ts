import { Entity, Column, PrimaryGeneratedColumn, Long } from 'typeorm';

@Entity()
export class Fare {
    @PrimaryGeneratedColumn()
    fare_id: Long

    @Column({ type: 'varchar', name: 'header', length: 20 })
    header: string;

    @Column({ type: 'varchar', name: 'fare_money', nullable: true })
    fareMoney: string;

    @Column({ type: 'varchar', name: 'detail', nullable: true })
    detail: string;

    @Column({ type: 'varchar', name: 'created_by', nullable: true })
    createdBy: string;

    @Column({ type: 'datetime', name: 'created_date', nullable: true })
    createdDate: Date;

    @Column({ type: 'varchar', name: 'updated_by', nullable: true })
    updatedBy: string;

    @Column({ type: 'datetime', name: 'updated_date', nullable: true })
    updatedDate: Date;


    @Column({ type: 'varchar', name: 'is_deleted', default: "N" })
    isDeleted: string;

}