import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './modules/auth/auth.module';
import { UsersModule } from './modules/users/users.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configurationDev from './config/configuration.dev';
import configurationProd from './config/configuration.prod';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { FileManagerModule } from './modules/file-manager/file-manager.module';
import { LeaveModule } from './modules/leave/leave.module';
import { FareModule } from './modules/fare/fare.module';
import { CvModule } from './modules/cv/cv.module';
import { HolidayModule } from './modules/holiday/holiday.module';
import { RegisterModule } from './modules/register/register.module';

@Module({
  imports: [
    AuthModule,
    UsersModule,
    ConfigModule.forRoot({
      load: [
        process.env.npm_lifecycle_event == 'start:dev' ?
          configurationDev : configurationProd
      ]
    }),
    TypeOrmModule.forRoot({
      autoLoadEntities: false,
    }),
    FileManagerModule,
    LeaveModule,
    FareModule,
    CvModule,
    HolidayModule,
    RegisterModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private connection: Connection) { }
}
