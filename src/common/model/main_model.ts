import { Column } from 'typeorm';

export class CommonColumn {

    @Column({ type: 'varchar', name: 'created_by', nullable: true })
    createdBy: string;

    @Column({ type: 'datetime', name: 'created_date', nullable: true })
    createdDate: Date = new Date();

    @Column({ type: 'varchar', name: 'updated_by', nullable: true })
    updatedBy: string;

    @Column({ type: 'datetime', name: 'updated_date', nullable: true })
    updatedDate: Date;

    @Column({ type: 'varchar', name: 'is_deleted', default: "N" })
    isDeleted: string;

}