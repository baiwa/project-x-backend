import * as nodemailer from 'nodemailer';

// async..await is not allowed in global scope, must use a wrapper
export const sendEmail = async (
    emailTo: string[],
    emailFrom: string,
    nameFrom: string,
    // nameTo: string,
    data: {
        subject: string,
        typeEmail: string,
        description: string,
        fileUpload?: FileUpload[],
    }
) => {
    // create reusable transporter object using the default SMTP transport
    const transporter = nodemailer.createTransport({
        host: 'smtp.sendgrid.net',
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            // baiwaApiKey
            user: 'apikey', // generated ethereal user
            pass: process.env.SENDGRID_API_KEY, // generated ethereal password
            // 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
        },
    });

    // send mail with defined transport object
    const info = await transporter.sendMail({
        from: '"BAIWA COMPANY LIMITED" <baiwadev2020@gmail.com>', // sender address
        to: emailTo, // list of receivers
        subject: data.subject, // Subject line
        text: data.subject, // plain text body
        html: `<p></p>
        <h1 style="text-align: center; ">
            <img src="https://scontent.fbkk8-4.fna.fbcdn.net/v/t1.0-1/p200x200/42507164_678882619150202_7706013391722119168_n.png?_nc_cat=100&amp;_nc_sid=dbb9e7&amp;_nc_eui2=AeE0wAggoS8_c0OXxklV03tukG8uD3B7kpGQby4PcHuSkSWpen-M1VuUzABh1OyicLJZj0IZoNHzHPSU1Xupyksq&amp;_nc_ohc=hVV8xlcrTtYAX_ISz_j&amp;_nc_ht=scontent.fbkk8-4.fna&amp;oh=6269cad19325911fc627264b035e3a8b&amp;oe=5EB454C2"
                style="width: 45.3125px; height: 45.3125px;">
            <span style="color: rgb(66, 81, 95); font-family: &quot;Arial Black&quot;; font-size: 20px;">บริษัท ใบวา จำกัด - BAIWA COMPANY LIMITED</span>
        </h1>
        <h1 style="text-align: center; ">
            <font color="#42515f" face="Arial Black">
                <span style="font-size: 20px; font-family: &quot;Courier New&quot;;">${data.typeEmail}</span>
            </font>
        </h1>
        <p style="text-align: center; "><br></p>
        <p style="text-align: left;">เรียน ผู้จัดการฝ่ายบุคคล</p>
        <p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;${data.description}</p>
        <p style="text-align: right;">ด้วยความเคารพอย่างสูง&nbsp; &nbsp; &nbsp;&nbsp;</p>
        <p style="text-align: right;">(${nameFrom})&nbsp; &nbsp; &nbsp;&nbsp;</p>
        <blockquote>
            <font size="1" color="#5f5f5f" face="sans-serif">
                <span style="font-family: Arial;">From: &nbsp; &nbsp; &nbsp; &nbsp;</span></font>
            <font size="1" face="sans-serif" style="color: rgb(51, 51, 51);"><span style="font-family: Arial;">${nameFrom} &lt;${emailFrom}&gt;<br></span></font>
            
         
            <font size="1" color="#5f5f5f" face="sans-serif">Date: &nbsp; &nbsp; &nbsp; &nbsp;</font>
            <font size="1" face="sans-serif" style="color: rgb(51, 51, 51);">${new Date()}<br></font><span style="color: rgb(95, 95, 95); font-family: sans-serif; font-size: x-small;">Company: บริษัท ใบวา จำกัด - BAIWA COMPANY LIMITED</span>
        </blockquote>
        <p></p>`,
        attachments: data.fileUpload || []
        // <font size="1" color="#5f5f5f" face="sans-serif">To: &nbsp; &nbsp; &nbsp; &nbsp;</font>
        // <font size="1" face="sans-serif" style="color: rgb(51, 51, 51);">${nameTo}<br></font>
    });
    // b2e2315a-602a-4b00-85b7-01cdfadc4c9d
    // W7oO7kH1wS8nW5yD2gU8pI4qL2sB4lK7hL7jS8vW0wD7yP5lS3

    // 6739ca58-1bb7-4ea7-90ae-476013c56920
    // lA2mS7nX7gH4rV5pR4xC4aJ8pS7bU2gN4iU3bK3cC8dD8hJ1fS

    console.log('Message sent: %s', info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
};

export interface FileUpload {
    // file name show in detail
    filename: string;
    // file path
    content: any;
}
