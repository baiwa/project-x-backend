

import { Controller, Get, UseGuards, Param, Post, ValidationPipe, UsePipes, Body } from '@nestjs/common';
import { CvService } from './cv.service';
import { CvReq } from './vo/req/cv.req';
import { JwtAuthGuard } from 'src/modules/auth/jwt-auth.guard';
import { User } from 'src/modules/users/users.decorator';
@UseGuards(JwtAuthGuard)
@Controller('cv')
export class CvController {

    constructor(
        private cvService: CvService,
    ) { }

    @Get('getall')
    getAllPro() {
        return this.cvService.findAll();
    }

    @Post('save')
    @UsePipes(ValidationPipe)
    cvReq(@User() user: any, @Body() req: CvReq) {
        return this.cvService.save(req, user);
        // console.log(req, user);
    }


    @Get('byname')
    getByName(@User() user: any) {
        return this.cvService.findbyname(user);
    }

    // @Get('user-by-id/:id')
    // getById(@Param('id') id: String) {
    //     return this.cvService.findById(id);
    // }


}
