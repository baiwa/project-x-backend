import { IsString } from 'class-validator';

export class CvReq {

    @IsString()
    filename: string;

    @IsString()
    nameInServer: string;
}