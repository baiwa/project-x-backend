
import { Injectable } from '@nestjs/common';
import { CV } from '../../models/cv.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CvReq } from './vo/req/cv.req';


@Injectable()
export class CvService {
    constructor(
        @InjectRepository(CV)
        private readonly cvRepository: Repository<CV>,
    ) { }

    async findOne(username: string): Promise<CV | undefined> {
        return this.cvRepository.findOne({ where: `username = '${username}' and is_deleted = 'N'` });
    }

    async findAll(): Promise<CV[]> {
        return await this.cvRepository.find({ where: "is_deleted = 'N'" });
    }

    async save(req: CvReq, user: any): Promise<any> {
        var data = new CV();
        data.username = user.username;
        data.filename = req.filename;
        data.nameInServer = req.nameInServer;
        data.createdBy = user.username;
        data.createdDate = new Date();
        this.cvRepository.save(data);

        return await this.cvRepository.find({ where: "is_deleted = 'N'" });
    }

    async findbyname(user: any) {
        return await this.cvRepository.query(

            `select  * from cv where created_by = '${user.username}' and  is_deleted = 'N' 
            order by created_date desc `
        );

    }

    // async findById(id: String) {
    //     return await this.cvRepository.findOne(6);
    // }


}
