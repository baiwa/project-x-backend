import { Injectable } from '@nestjs/common';
import { sendEmail, FileUpload } from 'src/utils/sendEmail';
import * as fs from 'fs';
import { LeaveEmail } from 'src/models/leave_email.entity';
import { SendToEmail } from 'src/models/send_to_email.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { SendEmailLeaveReq } from './vo/req/send_email_leave.req';
import { Admin } from '../../common/constant/rule_users';
import { FwUsersDetail } from 'src/models/fw_users_dtl.entity';
@Injectable()
export class LeaveService {
    constructor(
        @InjectRepository(LeaveEmail)
        private readonly LeaveEmailRepository: Repository<LeaveEmail>,
        @InjectRepository(SendToEmail)
        private readonly SendToEmailRepository: Repository<SendToEmail>,
        @InjectRepository(FwUsersDetail)
        private readonly fwUsersDetailRepository: Repository<FwUsersDetail>, ) { }

    async sendEmailLeave() {
        await this.example();
        try {
            return 'has Send';
        } catch (error) {
            return 'error ' + error;
        }
    }

    private async  example() {

        await sendEmail(
            ['pack_2541@hotmail.com'],
            'baiwadev2020@gmail.com',
            'watcharapong sribun',
            // 'chawean naja',
            {
                subject: 'test',
                description: 'des',
                typeEmail: 'ลาออก',
                fileUpload: [{ filename: 'filename.jpeg', content: fs.readFileSync("./baiwa_upload_file/TESTIMAGE.jpg") }]
            }
        )
        console.log('send');
    }

    async findAllByRule(userData: any): Promise<LeaveEmail[]> {
        try {

            return await this.LeaveEmailRepository.find({ where: `is_deleted = 'N' and created_by = '${userData.username}'`, order: { createdDate: "DESC" } });
        } catch (error) {
            return error
        }


    }
    async findAllByAdmin(userData: any): Promise<LeaveEmail[]> {
        console.log("-->", userData)
        var whereData = `is_deleted = 'N' and created_by = '${userData.username}'`;
        // this.LeaveEmailRepository.query(`
        // Select lev.*, dtl.email 
        // from leave_emails lev
        // join fw_users_detail dtl 
        //         on user.username=dtl.username
        // `)
        if (userData.userRole.toLowerCase() == Admin.toLowerCase()) {
            whereData = `is_deleted = 'N'`;
        }

        try {

            return await this.LeaveEmailRepository.find({ where: whereData, order: { createdDate: "DESC" } });
        } catch (error) {
            return error;
        }


    }

    async save(req: SendEmailLeaveReq, user: any): Promise<any> {
        var resUserData = await this.fwUsersDetailRepository.query(
            `Select dtl.email,dtl.phone,dtl.job_position,dtl.first_name,dtl.last_name
            from  fw_users_detail dtl             
             where dtl.is_deleted = 'N' and dtl.username =  '${user.username}' `
        );
        var data = new LeaveEmail();
        var fileList: FileUpload[] = [];

        data.title = req.title;
        data.sendBy = user.username;
        data.detail = req.detail;
        data.leaveType = req.leaveType;
        data.createdBy = user.username;
        if (null != req.fileUpload) {
            data.fileUpload = req.fileUpload.join();
            data.fileUploadName = req.fileUploadName.join();
            for (var index = 0; index < req.fileUpload.length; index++) {
                fileList[index] = {
                    content: fs.readFileSync("./baiwa_upload_file/" + req.fileUpload[index]),
                    filename: req.fileUploadName[index]
                }
            }
        } else {
            data.fileUpload = ''
            data.fileUploadName = ''
        }
        // data.sendTo = 'this field need to delete'
        data.startDate = req.startDate;
        data.dateType = req.dateType;
        data.endDate = req.endDate;
        data.createdDate = new Date();
        try {

            var queryData = ` and dtl.username = '${req.sendTo[0]}'`;
            for (var index = 1; index < req.sendTo.length; index++) {
                queryData += ` or dtl.username = '${req.sendTo[index]}' `
            }
            console.log(queryData)
            var emailDataSendList = await this.fwUsersDetailRepository.query(
                `Select dtl.email,dtl.phone,dtl.job_position,dtl.first_name,dtl.last_name
                from  fw_users_detail dtl             
                 where dtl.is_deleted = 'N'  ${queryData} `
            );
            var emailList = [];
            for (var index = 0; index < emailDataSendList.length; index++) {
                emailList.push(emailDataSendList[index].email)
            }
            console.log(emailList)
            await sendEmail(
                emailList,
                resUserData[0].email,
                '' + resUserData[0].first_name + ' ' + resUserData[0].last_name,
                {
                    subject: req.title,
                    description: req.detail,
                    typeEmail: req.leaveType,
                    fileUpload: fileList
                }
            )

            console.log('send');
            console.log(resUserData[0]);
            // return console.log('' + resUserData[0].first_name + ' ' + resUserData[0].last_name);


            let response = await this.LeaveEmailRepository.save(data);
            for (var index = 0; index < req.sendTo.length; index++) {
                var dataSendTo = new SendToEmail();
                dataSendTo.sendToUser = req.sendTo[index];
                dataSendTo.leaveEmailId = response.leave_email_id;
                let responseSendTo = await this.SendToEmailRepository.save(dataSendTo);
            }
            console.log('---save!!!!---', response.leave_email_id)
            return "save!!"
        } catch (error) {
            return error.message
        }

    }

}
