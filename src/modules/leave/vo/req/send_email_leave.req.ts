import { IsString, IsInt, IsArray, IsDate } from 'class-validator';
import { Long } from 'typeorm';
export class SendEmailLeaveReq {

    @IsInt()
    readonly fwleave_cc_id: Long;

    @IsArray()
    readonly sendTo: Array<string>;

    @IsArray()
    readonly fileUpload: Array<string>;

    @IsArray()
    readonly fileUploadName: Array<string>;

    @IsString()
    readonly title: string;

    @IsString()
    readonly leaveType: string;

    @IsString()
    readonly createdBy: string;

    @IsString()
    readonly detail: string;

    @IsString()
    dateType: string;

    @IsDate()
    startDate: Date;

    @IsDate()
    endDate: Date;
}