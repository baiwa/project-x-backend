import { Module } from '@nestjs/common';
import { LeaveController } from './leave.controller';
import { LeaveService } from './leave.service';

import { SendToEmail } from '../../models/send_to_email.entity';
import { LeaveEmail } from '../../models/leave_email.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FwUsersDetail } from 'src/models/fw_users_dtl.entity';

@Module({
  imports: [TypeOrmModule.forFeature([LeaveEmail, SendToEmail, FwUsersDetail])],
  controllers: [LeaveController],
  exports: [LeaveService],
  providers: [LeaveService],
})
export class LeaveModule { }
