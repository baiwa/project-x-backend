import { Controller, Get, Body, UsePipes, ValidationPipe, Post, UseGuards } from '@nestjs/common';
import { SendEmailLeaveReq } from './vo/req/send_email_leave.req';
import { LeaveService } from './leave.service';
import { JwtAuthGuard } from 'src/modules/auth/jwt-auth.guard';
import { User } from '../users/users.decorator';
@UseGuards(JwtAuthGuard)
@Controller('leave')
export class LeaveController {

    constructor(
        private leaveService: LeaveService
    ) { }

    @Get('getall')
    getAllByRule(@User() user: any) {
        return this.leaveService.findAllByRule(user);
    }

    @Get('getall-all')
    getAllByAdmin(@User() user: any) {
        return this.leaveService.findAllByAdmin(user);
    }
    @Get('testsend')
    testSendEmail() {
        return this.leaveService.sendEmailLeave();
    }

    @Post('save')
    save(@Body() req: SendEmailLeaveReq, @User() user: any) {
        return this.leaveService.save(req, user);
    }

    @Post('send-email-leave')
    // @UsePipes(ValidationPipe)
    sendEmailLeave(@Body() req: SendEmailLeaveReq) {
        return this.leaveService.sendEmailLeave();
    }
}
