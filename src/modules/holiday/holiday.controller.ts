import { Controller, Get, Param } from '@nestjs/common';
import { HolidayService } from './holiday.service';

@Controller('holiday')
export class HolidayController {
    constructor(private holidayService: HolidayService) { }

    @Get('sync/:year')
    async syncDay(@Param('year') year) {
        return await this.holidayService.getSync(year);
    }

    @Get('get/:year')
    async getHoliday(@Param('year') year) {
        return await this.holidayService.getHolidayList(year);
    }
}
