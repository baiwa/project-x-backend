import { Module, HttpModule } from '@nestjs/common';
import { HolidayService } from './holiday.service';
import { HolidayController } from './holiday.controller';
import { Holiday } from 'src/models/holiday.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [HttpModule, TypeOrmModule.forFeature([Holiday])],
  providers: [HolidayService],
  controllers: [HolidayController]
})
export class HolidayModule { }
