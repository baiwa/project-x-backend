import { Injectable, HttpService } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Holiday } from 'src/models/holiday.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class HolidayService {
    private clientId = '6739ca58-1bb7-4ea7-90ae-476013c56920';

    constructor(
        private httpService: HttpService,

        // Holiday Repo
        @InjectRepository(Holiday)
        private readonly holidayRepository: Repository<Holiday>,
    ) { }

    async  getSync(year: string) {
        let res = await this.httpService.get(
            `https://apigw1.bot.or.th/bot/public/financial-institutions-holidays/?year=${year}`
            , {

                headers: {
                    'accept': 'application/json',
                    'x-ibm-client-id': this.clientId
                },
            }
        ).toPromise();

        let dataSet: Holiday;
        let data = res.data.result.data;
        let numSave = 0;
        for (let index = 0; index < data.length; index++) {

            data[index].Date += ' 00:00:00.0'

            dataSet = new Holiday();
            dataSet.createdBy = 'SYSTEM';
            dataSet.holidayDay = data[index].Date;
            dataSet.holidayDescription = data[index].HolidayDescriptionThai;

            const dataFind = await this.holidayRepository.find({ where: ` holiday_day = '${dataSet.holidayDay}' and  created_by = 'SYSTEM' ` });
            if (dataFind.length == 0) {
                await this.holidayRepository.save(dataSet);
                numSave++;
            }
        }
        return `complete save ${numSave}`;
    }

    getHolidayList(year: string) {
        return this.holidayRepository.find({ where: `holiday_day BETWEEN '${year}-01-01' AND '${year}-12-31' and active='Y' and is_deleted ='N' `, order: { holidayDay: 'ASC' } });
    }
}
