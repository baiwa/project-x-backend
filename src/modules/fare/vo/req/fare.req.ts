import { IsString } from 'class-validator';

export class FareReq {

    @IsString()
    header: string;

    @IsString()
    fareMoney: string;

    @IsString()
    detail: string;
}