import { Module } from '@nestjs/common';
import { FareController } from './fare.controller';
import { FareService } from './fare.service';
import { Fare } from 'src/models/fare.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Fare])],
  controllers: [FareController],
  providers: [FareService]
})
export class FareModule { }
