import { Controller, Get, UseGuards, Body, UsePipes, Post, ValidationPipe } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { FareService } from './fare.service';
import { FareReq } from './vo/req/fare.req';
import { User } from 'src/modules/users/users.decorator';



@UseGuards(JwtAuthGuard)
@Controller('fare')
export class FareController {

    constructor(
        private fareService: FareService,
    ) { }

    @Get('getall')
    getAllPro() {
        return this.fareService.findAll();
    }

    @Post('save')
    @UsePipes(ValidationPipe)
    cvReq(@User() user: any, @Body() req: FareReq) {
        return this.fareService.save(req, user);
        // console.log(req, user);
    }

    @Get('byname')
    getByName(@User() user: any) {
        return this.fareService.findbyname(user);
    }

    @Get('getall-admin')
    getAllByAdmin(@User() user: any) {
        return this.fareService.findAllByAdmin(user);
    }
}

