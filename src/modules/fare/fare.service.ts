import { Injectable } from '@nestjs/common';
import { Fare } from 'src/models/fare.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FareReq } from './vo/req/fare.req';
import { Admin } from '../../common/constant/rule_users';

@Injectable()
export class FareService {

    constructor(
        @InjectRepository(Fare)
        private readonly fareRepository: Repository<Fare>,
    ) { }

    async findAll(): Promise<Fare[]> {
        return await this.fareRepository.find({ where: "is_deleted = 'N'" });
    }


    async save(req: FareReq, user: any): Promise<any> {
        var data = new Fare();
        data.header = req.header;
        data.fareMoney = req.fareMoney;
        data.detail = req.detail;
        data.createdBy = user.username;
        data.createdDate = new Date();

        this.fareRepository.save(data);
        return await this.fareRepository.find({ where: "is_deleted = 'N'" });

    }

    async findbyname(user: any) {
        return await this.fareRepository.query(

            `select  * from fare where created_by = '${user.username}' and  is_deleted = 'N' 
            order by created_date desc `
        );

    }

    async findAllByAdmin(userData: any): Promise<Fare[]> {

        var whereData = `is_deleted = 'N' and created_by = '${userData.username}'  `;
        var fareMonth = await this.fareRepository.query(`select sum(fare_money) AS faremonth FROM fare where created_by = '${userData.username}'
        and  is_deleted = 'N' `);
        var fareYear = '10000';

        if (userData.userRole.toLowerCase() == Admin.toLowerCase()) {
            whereData = `is_deleted = 'N'`;
            fareMonth = `is_deleted = 'N'`;
            fareYear = `is_deleted = 'N'`;
        }
        return await this.fareRepository.find({ where: fareMonth, order: { createdDate: "DESC" } });


    }

}
