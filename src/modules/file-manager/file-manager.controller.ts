import { Controller, Post, UseInterceptors, UploadedFiles, UseGuards, Param, UploadedFile, Get, Header, Res, HttpStatus, Query, Delete } from '@nestjs/common';
import { AnyFilesInterceptor, FileInterceptor } from '@nestjs/platform-express';
import { FileManagerService } from './file-manager.service';
import { JwtAuthGuard } from 'src/modules/auth/jwt-auth.guard';
import { extname } from 'path';
import { diskStorage } from 'multer';
import { Response } from 'express';
// @UseGuards(JwtAuthGuard)
@Controller('file-manager')
export class FileManagerController {

    constructor(private fileManagerService: FileManagerService) { }

    @UseGuards(JwtAuthGuard)
    @Post('uploads')
    @UseInterceptors(
        AnyFilesInterceptor(
            {
                storage: diskStorage({
                    destination: './baiwa_upload_file',
                    filename: (req, file, cb) => {
                        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                        return cb(null, `${randomName}${extname(file.originalname)}`)
                    }
                })
            }
        ))
    uploadFiles(@UploadedFiles() files: Array<any> = []) {
        let listFile: Array<String> = new Array<String>();
        files.forEach(item => {
            listFile.push(item.path.replace('baiwa_upload_file/', '').replace('baiwa_upload_file\\', ''));
        });
        return listFile;
    }

    @Get('get-img/:name')
    async getImage(@Param('name') name: string, @Query() query, @Res() res: Response) {
        res.contentType('png')
            .end(await this.fileManagerService.getImage(name, query));
        return res;
    }

    @UseGuards(JwtAuthGuard)
    @Delete('delete/:name')
    async deleteImage(@Param('name') name: string) {
        this.fileManagerService.deleteFile(name);
    }

    // @UseGuards(JwtAuthGuard)
    @Get('download-img/:name')
    async download(@Param('name') name: string, @Res() res: Response) {
        res.setHeader('Content-disposition', 'attachment; filename=' + name);
        res.end(await this.fileManagerService.download(name));
        return res;
    }
}
