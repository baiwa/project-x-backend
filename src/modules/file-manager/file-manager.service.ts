import { Injectable, UploadedFiles, Res } from '@nestjs/common';
import * as fs from 'fs';
import * as sharp from 'sharp';

@Injectable()
export class FileManagerService {
    filePath = './baiwa_upload_file/';

    uploadFile(@UploadedFiles() file): Array<String> {
        let listFileId: Array<String> = new Array();
        if (!fs.existsSync(this.filePath)) {
            fs.mkdirSync(this.filePath);
        }
        file.forEach(item => {
            let uuid: String = this.generateUniqueFileId();
            console.log(item);
            // fs.copyFileSync(item, '../uploadFileBaiwa');
            listFileId.push(uuid);
        });
        return listFileId;
    }

    private generateUniqueFileId(): String {
        let dt = new Date().getTime();
        const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            const r = (dt + Math.random() * 16) % 16 | 0;
            dt = Math.floor(dt / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    }

    async getImage(imgName: string, query) {
        let semiTransparentRedPng;
        if (query?.height > 0 && query?.width > 0) {
            semiTransparentRedPng = await sharp(this.filePath + imgName).resize(Number(query?.width), Number(query?.height)).png().toBuffer();
        } else {
            semiTransparentRedPng = await sharp(this.filePath + imgName).png().toBuffer();
        }
        return semiTransparentRedPng;
    }

    async deleteFile(filename: string) {
        fs.unlinkSync(this.filePath + filename);
    }

    async download(filename: string) {
        return fs.readFileSync(this.filePath + filename);
    }
}
