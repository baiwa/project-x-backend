import { Controller, Get, UseGuards, Post, UseInterceptors, UploadedFiles } from '@nestjs/common';
import { UsersService } from './users.service';
import { JwtAuthGuard } from 'src/modules/auth/jwt-auth.guard';
import { User } from "./users.decorator";
import { AnyFilesInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { extname } from 'path';
import { diskStorage } from 'multer';
import { FileManagerService } from 'src/modules/file-manager/file-manager.service';
import { AuthGuard } from '@nestjs/passport';

@UseGuards(JwtAuthGuard)
@Controller('users')
export class UsersController {
    constructor(
        private userService: UsersService,
        private fileManagerService: FileManagerService
    ) { }

    @Get('getall')
    getAllPro() {
        return this.userService.findAll();
    }

    @Get('getall-dtl')
    getAllDtl(@User() user: any) {
        return this.userService.findAllJoinDtl(user);
    }

    @Get('user-by-id')
    getById(@User() data: any) {
        return this.userService.findById(data.username);
    }

    @Post('upload-img-profile')
    @UseInterceptors(
        AnyFilesInterceptor(
            {
                storage: diskStorage({
                    destination: './baiwa_upload_file',
                    filename: (req, file, cb) => {
                        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                        return cb(null, `${randomName}${extname(file.originalname)}`)
                    }
                })
            }
        ))
    async uploadFiles(@UploadedFiles() files: Array<any> = [], @User() user: any) {
        let listFile: String = new String;
        listFile = files[0].path.replace('baiwa_upload_file/', '').replace('baiwa_upload_file\\', '');
        var userData = await this.userService.findImageById(user);
        if (null != userData.imgProfile)
            this.fileManagerService.deleteFile(userData.imgProfile);
        return this.userService.uploadImageProfile(listFile, user);
    }

    @Get()
    async findOne(@User() user: any) {
        console.log(user);
    }

}
