import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { LocalAuthGuard } from 'src/modules/auth/local-auth.guard';
import { JwtAuthGuard } from 'src/modules/auth/jwt-auth.guard';

export function Auth(...roles: String[]) {
    console.log("aaa", roles);
    return applyDecorators(
        SetMetadata('roles', roles),
        // UseGuards(AuthGuard, JwtAuthGuard),
        // ApiBearerAuth(),
        // ApiUnauthorizedResponse({ description: 'Unauthorized"' }),
    );
}