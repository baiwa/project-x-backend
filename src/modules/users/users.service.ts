import { Injectable } from '@nestjs/common';
import { FwUsers } from '../../models/fw_users.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FwUsersDetail } from '../../models/fw_users_dtl.entity';
import { FwUsersImgProfile } from '../../models/fw_users_img_profile.entity';

@Injectable()
export class UsersService {

    constructor(
        @InjectRepository(FwUsers)
        private readonly fwUsersRepository: Repository<FwUsers>,
        @InjectRepository(FwUsersDetail)
        private readonly fwUsersDetailRepository: Repository<FwUsersDetail>,
        @InjectRepository(FwUsersImgProfile)
        private readonly fwUsersImgProfileRepository: Repository<FwUsersImgProfile>,
    ) { }

    async findOne(username: string): Promise<FwUsers | undefined> {
        return this.fwUsersRepository.findOne({ where: `username = '${username}' and is_deleted = 'N'` });
    }

    async findAll(): Promise<FwUsers[]> {
        return await this.fwUsersRepository.find({ where: "is_deleted = 'N'" });
    }

    async findAllJoinDtl(user:any) {
        return await this.fwUsersRepository.query(
            `Select user.username ,dtl.company,dtl.email,dtl.phone,dtl.job_position,dtl.first_name,dtl.last_name,img.img_profile 
            from fw_users user
                join fw_users_detail dtl 
                on user.username=dtl.username
                join fw_users_img_profile img
                on user.username =img.username 
            where user.is_deleted = 'N' and dtl.is_deleted = 'N' and user.username != '${user.username}'
            order by dtl.email asc `
        );
    }

    async findById(id: string): Promise<FwUsersDetail> {
        let user = await this.fwUsersRepository.query(
            `Select user.username ,user.user_rule,dtl.first_name,dtl.last_name,dtl.company,dtl.email,dtl.phone,
            dtl.job_position,img.img_profile 
            from fw_users user
            join fw_users_detail dtl 
            on user.username=dtl.username
            inner join fw_users_img_profile img
            on user.username =img.username 
        where user.username='${id}' and user.is_deleted = 'N' and dtl.is_deleted = 'N'`
        );

        return user[0];
    }

    async findImageById(user: any): Promise<FwUsersImgProfile> {
        return this.fwUsersImgProfileRepository.findOne({ where: `username='${user.username}'` });
    }


    async uploadImageProfile(files: any, user: any) {
        var dataUser = await this.fwUsersImgProfileRepository.findOne({ where: `username='${user.username}'` });
        dataUser.imgProfile = files;
        dataUser.updatedBy = user.username;
        dataUser.updatedDate = new Date();
        this.fwUsersImgProfileRepository.save(dataUser);
        return dataUser.imgProfile;
    }
}
