import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FwUsers } from '../../models/fw_users.entity';
import { UsersController } from './users.controller';
import { FwUsersDetail } from '../../models/fw_users_dtl.entity';
import { FwUsersImgProfile } from '../../models/fw_users_img_profile.entity';
import { FileManagerService } from 'src/modules/file-manager/file-manager.service';

@Module({
  imports: [TypeOrmModule.forFeature([FwUsers, FwUsersDetail, FwUsersImgProfile])],
  providers: [UsersService, FileManagerService],
  exports: [UsersService, FileManagerService],
  controllers: [UsersController],
})
export class UsersModule { }
