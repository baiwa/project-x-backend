import { IsString, IsInt, IsArray } from 'class-validator';
import { Long } from 'typeorm';
export class UserDetailReq {

    @IsInt()
    readonly fwuser_dtl_id: Long;

    @IsString()
    readonly username: string;
    
    @IsString()
    readonly email: string;

    @IsString()
    readonly company: string;

    @IsString()
    readonly phone: string;

    @IsString()
    readonly firstName: string;

    @IsString()
    readonly lastName: string;

    @IsString()
    readonly job_position: string;


}