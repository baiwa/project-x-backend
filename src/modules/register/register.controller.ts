import { Controller, Post, ValidationPipe, UsePipes } from '@nestjs/common';
import { RegisterReq } from './vo/req/register_req';
import { RegisterService } from './register.service';

@Controller('register')
export class RegisterController {

    constructor(private registerService: RegisterService) { }

    @Post()
    @UsePipes(ValidationPipe)
    async register(reqRegister: RegisterReq): Promise<String> {
        return await this.registerService.register(reqRegister);
    }
}
