import { IsString } from 'class-validator';

export class RegisterReq {

    @IsString()
    readonly username: string;

    @IsString()
    readonly user_rule: string;

    @IsString()
    readonly password: string;

    @IsString()
    readonly first_name: string;

    @IsString()
    readonly last_name: string;

    @IsString()
    readonly phone: string;

    @IsString()
    readonly email: string;

    @IsString()
    readonly job_position: string;

    @IsString()
    readonly company: string;

}