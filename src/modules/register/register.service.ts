import { Injectable } from '@nestjs/common';
import { RegisterReq } from './vo/req/register_req';
const crypto = require('crypto');

@Injectable()
export class RegisterService {

    private secretKey = 'ILoveLongTuNaja';

    public register(reqRegister: RegisterReq): string {
        const password = 'password';
        console.log(this.makeHase(password, ')cjHE0$&m_').hash);
        console.log(this.makeHase(password, ')cjHE0$&m_').salt);

        console.log(this.checkAuthen('b452b72d10ae80ad8c8616df353447be21d348db4e2fd0a4ac665d05246110ebde5a52ddd62e15e24fdedeadc3241cec0ba0d634eb066055c011323e9f646e2725b9bc4ae5784d545b4d6efd8b685b219a7e99d47ea5151083b703083951f170f8480ccbeac6240ac8d99c0be5e893389a8af0f1e6b60469c00c823578ec5756', { salt: ')cjHE0$&m_', password: password }));

        return '';
    }



    private makeHase(password: string, saltReq?: string): MakeHase {
        const salt = saltReq || this.makeSalt(10);
        const passwordLength = password.length;

        password =
            password.substring(0, passwordLength / 2)
            + passwordLength
            + this.secretKey
            + password.substring(passwordLength / 2, password.length);

        const hash = crypto.pbkdf2Sync(password, salt, 100000, 128, 'sha512').toString('hex');

        return { hash: hash, salt: salt };
    }

    private makeSalt(length: number) {
        var result = '';
        var characters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+ๅ/_ภถุึคตจขชผปแอิืทมใฝงวสา่้ดดหหฟ';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    public checkAuthen(hashPasswordOld: string, passwordReq: MakeHase): boolean {
        const hashReq = this.makeHase(passwordReq.password, passwordReq.salt);
        if (hashReq.hash === hashPasswordOld) {
            return true;
        }
        return false;
    }
}

interface MakeHase {
    hash?: string;
    password?: string;
    salt: string;
}
